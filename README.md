# Abstract
Transfomer + ChannelAttention + LSTMAttention  

# Useage  
- Clone this repository:
```shell
git clone https://github.com/ShiroDoMain/Attention_LSTM_RUL_Prediction.git
```  
- install requirements
```shell
pip install -r requirements.txt
```  
- run
```shell
cd Attention_LSTM_RUL_Prediction
python3 train.py
```